# Eksamen 2018
[TOC]

## Antakelser


## Kommentarer
* Når det gjelder oppgave 3 så brukte jeg for lang tid på databasefunksjonaliteten, og var nødt til å be om løsningsforslaget etterhvert. Etter at jeg hadde fått løsningsforslaget så viste det seg at jeg var ganske nerme riktig løsning, men endte opp men en refaktorering av koden til det værre rett før jeg ble tilsendt løsningsforslaget
* I Oppgave 4 så brukes det funksjonalitet som brukes opp igjen i oppgave 5 for å opprette databasen. Her kunne man ha brukt koden fra oppgave 4 opp igjen for å løse oppgave 5. I dette tilfellet så lot jeg det være to separerte klasser siden det er to forskjellige oppgaver. 
* En del av kommenteringen og navngiving av variable er litt om hverandre på Norsk og Engelsk. Er klar over at dette kan gjøre koden litt forvirrende å lese.
