CREATE TABLE places (
  placeid int NOT NULL GENERATED ALWAYS AS IDENTITY,
  kommunenr int NOT NULL,
  stedsnavn varchar(100) NOT NULL,
  stedstype varchar(100) NOT NULL,
  kommune varchar(100) NOT NULL,
  fylke varchar(100) NOT NULL,
  varselurl varchar(200) NOT NULL,
  latitude double NOT NULL,
  longtitude double NOT NULL)