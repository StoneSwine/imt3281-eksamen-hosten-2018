package no.ntnu.imt3281.yr_places;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Klasse som inneholder informasjon om værdatainformasjon på et sted
 * @author stoneswine
 *
 * Oppgave 1
 */
public class Place {
    int kommunenr;
    String stedsnavn;
    String stedstype;
    String kommune;
    String fylke;
    String varselurl;
    double latitude;
    double longtitude;

    public Place(){}

    /**
     * Constructur som tar en liste med stedsinformasjon og legger det inn i riktig variabel
     *
     * format of the list:
     *     0            1       2       3        4         5        6       7       8     9     11     12
     * kommunenr - stedsnavn - junk - junk - stedstype - junk - kommune - fylke - lat - long - url - junk?
     *
     * @param stedliste Liste med værdata
     */
    public Place(List<String> stedliste) {
        String nynorskurl;
        String bokmålurl;

        this.kommunenr = Integer.parseInt(stedliste.get(0));
        this.stedsnavn = stedliste.get(1);
        this.stedstype = stedliste.get(4);
        this.kommune = stedliste.get(6);
        this.fylke = stedliste.get(7);
        this.latitude = Double.parseDouble(stedliste.get(8));
        this.longtitude = Double.parseDouble(stedliste.get(9));

        nynorskurl = stedliste.get(11);
        bokmålurl = stedliste.get(12);

        //Se om en gyldig bokmålsurl finnes
        if(bokmålurl.contains(".xml")){
            this.varselurl = bokmålurl;
        }else {//bokmålsurl finnes ikke
            this.varselurl = nynorskurl;
        }
    }

    public Place(ResultSet res) throws SQLException {
        kommunenr = res.getInt(1);
        stedsnavn = res.getString(2);
        stedstype = res.getString(3);
        kommune = res.getString(4);
        fylke = res.getString(5);
        latitude = res.getDouble(6);
        longtitude = res.getDouble(7);
        varselurl = res.getString(8);
    }

    /**
     * Returnerer kommunenr til det gjeldene stedet
     * @return kommunenr
     */
    public int getKommunenr() {
        return kommunenr;
    }

    /**
     * Returnerer stedsnavnet til get gjeldene stedet
     * @return stedsnavn
     */
    public String getStedsnavn() {
        return stedsnavn;
    }

    /**
     * Returnerer stedstypen til det gjeldene stedet (Krike.. el.)
     * @return stedstype
     */
    public String getStedstype() {
        return stedstype;
    }

    /**
     * Returnerer komunenavnet til det gjeldene stedet
     * @return kommune
     */
    public String getKommune() {
        return kommune;
    }

    /**
     * Returnerer fylket til det gjeldene stedet
     * @return fylke;
     */
    public String getFylke() {
        return fylke;
    }

    /**
     * Returnerer breddegraden til det gjeldene stedet
     * @return latitude
     */
    public double getLat() {
        return latitude;
    }

    /**
     * Returnerer lengdegraden til det gjeldene stedet
     * @return longtitude
     */
    public double getLng() {
        return longtitude;
    }

    /**
     * Returnerer URL'en til YR.NO hvor dette stedet er lokalisert
     * @return varselurl
     */
    public String getVarselURL() {
        return varselurl;
    }

    public void setKommunenr(int kommunenr) {
        this.kommunenr = kommunenr;
    }

    public void setStedsnavn(String stedsnavn) {
        this.stedsnavn = stedsnavn;
    }

    public void setStedstype(String stedstype) {
        this.stedstype = stedstype;
    }

    public void setKommune(String kommune) {
        this.kommune = kommune;
    }

    public void setFylke(String fylke) {
        this.fylke = fylke;
    }

    public void setVarselurl(String varselurl) {
        this.varselurl = varselurl;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

}
