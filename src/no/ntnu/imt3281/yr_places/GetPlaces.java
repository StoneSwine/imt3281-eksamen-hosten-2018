package no.ntnu.imt3281.yr_places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Downloads a file with all the data about the places and puts them into a list
 * containing list elements with the data
 *
 * Oppgave 2
 */
public class GetPlaces {
    /**
     * Download all the data and put it into a list
     */
    public static List<List<String>>hentSteder() {
        String linje;
        List<List<String>> steder = new ArrayList<>();
        List<String> sted;
        try{

            //This snippet is collected from:
            // https://stackoverflow.com/questions/6259339/how-to-read-a-text-file-directly-from-internet-using-java
            URL url = new URL("http://fil.nrk.no/yr/viktigestader/noreg.txt");
            BufferedReader inputurl = new BufferedReader(new InputStreamReader(url.openStream()));

            //Loope gjennom alle stedene('sted') og legge de til i listen over alle stedene
            while ((linje = inputurl.readLine()) != null) {
                sted = new ArrayList<>(Arrays.asList(linje.split("\t")));
                steder.add(sted);
            }

            inputurl.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return steder;
    }
}
