package no.ntnu.imt3281.Runnable;

import no.ntnu.imt3281.logic.DataStore;
import no.ntnu.imt3281.yr_places.GetPlaces;
import no.ntnu.imt3281.yr_places.Place;

import java.sql.SQLException;
import java.util.List;

/**
 * Oppgave 4 --> Download data from website and store it in a database on disk
 */
public class DownloadData {

    private static DataStore db;
    /**
     * Test the permanent data store
     */
    public static void main(String[] args){
        Place place = null;
        try {
            db = DataStore.getDataStore();

            //Populate the Database
            List<List<String>> places = GetPlaces.hentSteder();
            for (int i = 1; i < places.size(); i++) {
                place = new Place(places.get(i));
                db.addPlace(place);
            }

            //Print the number of places in the database
            System.out.printf("Antall steder i databasen er %d", db.getNumberOfRows());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
