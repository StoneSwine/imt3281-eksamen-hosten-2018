package no.ntnu.imt3281.logic;

import no.ntnu.imt3281.yr_places.Place;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database_attempt {
    private static Connection connection;

    private Database_attempt(boolean memory){
        String connectionformat;
        if(memory){
            connectionformat = "jdbc:derby:memory:placesDB;create=true";
        }else{
            connectionformat = "jdbc:derby:placesDB;create=true";
        }

        try {
            this.connection = DriverManager.getConnection(connectionformat);
        }catch(SQLException sqlexception) {
            sqlexception.printStackTrace();
        }
    }

    private void CreateSchema() {
        File file = new File("src/no/ntnu/imt3281/resources/places.sql");

        try (Statement stmnt = connection.createStatement(); InputStream placetable = new FileInputStream(file);
             BufferedReader placetablebr = new BufferedReader(new InputStreamReader(placetable))) {

            String placetablestring = "";
            StringBuilder placetablesb = new StringBuilder();
            String placetableline = placetablebr.readLine();

            while (placetableline != null) {
                placetablesb.append(placetableline);
                placetablesb.append(System.lineSeparator());
                placetableline = placetablebr.readLine();
            }

            placetablestring = placetablesb.toString();

            stmnt.execute(placetablestring);

            placetable.close();
            placetablebr.close();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create new database in memory
     * @return database object
     */
    public static Database_attempt getTempDatabase(){
        Database_attempt db = new Database_attempt(true);
        db.CreateSchema();
        return db;
    }

    /**
     * Create new database on disk
     * @return databaseoject
     */
    public static Database_attempt getDiskbasedDatabase(){
        Database_attempt db = new Database_attempt(false);
        db.CreateSchema();
        return db;
    }

    /**
     * Add a new place to the database
     * @param place the placeoject to add
     */
    public void insertNewPlace(Place place){

        try(PreparedStatement prep = connection.prepareStatement("INSERT INTO places (kommunenr, stedsnavn, stedstype, kommune, fylke, varselurl, latitude, longtitude) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")){

            prep.setInt(1, place.getKommunenr());
            prep.setString(2, place.getStedsnavn());
            prep.setString(3, place.getStedstype());
            prep.setString(4, place.getKommune());
            prep.setString(5, place.getFylke());
            prep.setString(6, place.getVarselURL());
            prep.setDouble(7, place.getLat());
            prep.setDouble(8, place.getLng());

            prep.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the place closest to the X and Y coordinates
     * @param x the latitude
     * @param y the longtitude
     * @return the place oject closet to 'x' and 'y'
     */
    public Place findClosestPlace(double x, double y){
        Place place = null;
        Place resPlace = null;
        try(PreparedStatement prep = connection.prepareStatement("SELECT * FROM places")){

            ResultSet result =  prep.executeQuery();

            List<String> closestplace = new ArrayList<>();
            List<Place> places = null;
            while(result.next()) {

                place = new Place();

                closestplace = new ArrayList<>();
                place.setKommunenr(result.getInt("kommunenr"));
                place.setStedsnavn(result.getString("stedsnavn"));
                place.setStedstype(result.getString("stedstype"));
                place.setKommune(result.getString("kommune"));
                place.setFylke(result.getString("fylke"));
                place.setVarselurl(result.getString("varselurl"));
                place.setLatitude(result.getDouble("latitude"));
                place.setLongtitude(result.getDouble("longtitude"));

                places.add(place);
            }
            //Snipped below is taken from:
                //https://stackoverflow.com/questions/38853648/find-nearest-location-in-an-arraylist-from-given-float-values
            Double minDist = Double.MAX_VALUE;
            for (Place p: places) {
                double dist = Math.pow(p.getLat() - x, 2) + Math.pow(p.getLng() - y, 2);
                if (dist < minDist){
                    minDist = dist;
                    resPlace = p;
                }
            }

            result.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resPlace;
    }

    public int getNumberOfRows() {
        int numberofrows = 0;
        ResultSet result;
        try(PreparedStatement prep = connection.prepareStatement("SELECT COUNT(*) FROM places")){

        result = prep.executeQuery();
        while(result.next()){
            numberofrows++;
        }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberofrows;
    }
}
