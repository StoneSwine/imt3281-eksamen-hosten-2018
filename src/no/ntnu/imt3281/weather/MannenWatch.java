package no.ntnu.imt3281.weather;


import javafx.scene.control.Label;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Oppgave 7
 */
public class MannenWatch extends Label {
    /**
     * Constructor which checks if 'mannen' is still up or not and puts the result in a label
     */
    public MannenWatch(){
        try(BufferedReader input = new BufferedReader(new InputStreamReader(new URL("https://www.harmannenfalt.no/").openStream()))) {

            String line;
            //Search for the line 'yesnomaybe' and set it to the label
            while ((line = input.readLine()) != null){
                if(line.contains("yesnomaybe")){
                    super.setText(String.format("Har mannen falt: %s", input.readLine()));
                    return;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
