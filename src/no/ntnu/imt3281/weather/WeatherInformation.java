package no.ntnu.imt3281.weather;

import jdk.nashorn.api.scripting.URLReader;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Oppgave 6
 */
public class WeatherInformation {
    /**
     * Gets the forecast from the XML file
     * @param url the url to the XML file
     * @return a list of all the title and the body in the xml file
     */
    public static List<String> getForecast(String url){
        List<String> forecastlist = new ArrayList<>();
        //This is inspired from:
            //https://www.journaldev.com/1194/java-xpath-example-tutorial
        try{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new URL(url).openStream());

            //Get all the nodes with the right tags
            NodeList titlenodes = doc.getElementsByTagName("title");
            NodeList bodynodes =  doc.getElementsByTagName("body");

            //Add the nodes to the list:
                // title - body - title - body ...etc...
            for (int i = 0; i < titlenodes.getLength(); i++) {
                forecastlist.add(titlenodes.item(i).getTextContent());
                forecastlist.add(bodynodes.item(i).getTextContent());
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return forecastlist;
    }
}
