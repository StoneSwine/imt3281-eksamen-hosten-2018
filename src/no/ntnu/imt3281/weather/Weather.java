package no.ntnu.imt3281.weather;

import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import no.ntnu.imt3281.logic.DataStore;
import no.ntnu.imt3281.yr_places.GetPlaces;
import no.ntnu.imt3281.yr_places.Place;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Oppgave 5 og 6
 */
public class Weather {

    @FXML
    public WebView map;
    @FXML
    public WebView forecast;
    @FXML
    public BorderPane bp;

    private DataStore db;

    /**
     * Initialize the FXML file with the map
     */
    public void initialize() {
        downloadDataToDatabase();
        MannenWatch mannen = new MannenWatch();
        WebEngine engine = map.getEngine();
        engine.load("http://folk.ntnu.no/oeivindk/imt3281/map/pseudoMap.html");
        engine.setOnAlert(event -> findLocation(event.getData()));

        bp.setBottom(mannen);
    }

    private void downloadDataToDatabase() {
        Place place = null;
        try {
            db = DataStore.getDataStore();

            //Populate the database
            List<List<String>> places = GetPlaces.hentSteder();
            for (int i = 1; i < places.size(); i++) {
                place = new Place(places.get(i));
                db.addPlace(place);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Oppgave 5 og 6
     * @param data the coordinates to the location, separated by a '\t'
     */
    private void findLocation(String data) {
        StringBuilder html = null;
        List<String> values = Arrays.asList(data.split("\t"));
        Place place = getPlace(Double.parseDouble(values.get(0)), Double.parseDouble(values.get(1)));

        System.out.printf("Klikket på: %s som ligger i %s kommune%n", place.getStedsnavn(), place.getKommune());

        //List of the forecast:
        //Format:
        //      title - body - title - body ..etc..
        List<String> listofforecast = WeatherInformation.getForecast(place.getVarselURL());

        //Create and HTML string and print it out in the second webview
        WebEngine forecastengine = forecast.getEngine();
        html = new StringBuilder("<h2>" + place.getStedsnavn() + " i " + place.getKommune() + "</h2>");
        html.append("</br>");
        for (String aListofforecast : listofforecast) {
            html.append("</br>");
            html.append(aListofforecast);
            html.append("</br>");
        }
        forecastengine.loadContent(html.toString());
    }

    /**
     * Oppgave 6
     * @param x the X coordinate
     * @param y the Y coordinate
     * @return the Place object for the closest place in the database
     */
    private Place getPlace(double x, double y) {
        try {
            return db.getClosestPlace(x,y);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
