package no.ntnu.imt3281.click_areas;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.json.JSONArray;
import org.json.JSONObject;

import javafx.scene.paint.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javafx.scene.shape.Polygon;

import javax.swing.*;

/**
 * Oppgave 8
 * @author Stoned
 *
 */
public class CreateClickAreas {

    //FXML VARIABLE
    @FXML
    public ListView<Point> points;
    @FXML
    public TextField areaName;
    @FXML
    public TextArea areas;
    @FXML
    public AnchorPane imageContainer;

    //KLASSEVARIABLE
    //List over the current point
    private ObservableList<Point> currentpoint = FXCollections.observableArrayList();

    private JSONArray allpoints = new JSONArray();
    //This is the shape in FX
    private Polygon currentshape = new Polygon();

    /**
     * Setup before the GUI is displayed
     */
    public void initialize() {
        drawShape();
        //Make the ListView updated with the Observablelist
        points.setItems(currentpoint);
    }
    /**
     * Knappen "Legg til" i programmet
     * Legger til currentpoint inn i en jsonarray
     * @param event
     */
    public void addArea(ActionEvent event) {
        String area = areaName.getText();

        if(area.length() == 0) {
            return;
        }else {//Proceed if the shape has a name
            //Convert all the points in the list to jsonarray
            JSONArray points = convertPointsToJSON();

            //Assemble the object and put it into the main jsonarray
            JSONObject outerobject = new JSONObject();
            outerobject.put("polygon", points);
            outerobject.put("name", area);
            allpoints.put(outerobject);
        }

        //Print the jsonarray to the user
        areas.setText(allpoints.toString(2));

        //Reset
        areaName.clear();
        currentpoint.clear();
        imageContainer.getChildren().removeAll(currentshape);
        drawShape();
    }

    /**
     * Converts a List of Points to a jsonarray
     * @return the arraylist containing all the points in the current point
     */
    private JSONArray convertPointsToJSON() {
        JSONArray currentpointcord = new JSONArray();

        for (Point p : currentpoint){
            JSONObject jsonpoint = new JSONObject();

            jsonpoint.put("x", p.getX());
            jsonpoint.put("y", p.getY());

            currentpointcord.put(jsonpoint);
        }

        return currentpointcord;
    }

    /**
     * Skriver strukturen til fil
     * Jsonarray til fil
     * @param event
     */
    public void toFile(ActionEvent event) {
        //This snippet is from:
                //https://stackoverflow.com/questions/14589386/how-to-save-file-using-jfilechooser-in-java
        final JFileChooser fc = new JFileChooser();

        //Open the filechooser in the current working directory
        File workingDirectory = new File(System.getProperty("user.dir"));
        fc.setCurrentDirectory(workingDirectory);

        int retrival = fc.showSaveDialog(null);
        if (retrival == JFileChooser.APPROVE_OPTION) {
            try (FileWriter fw = new FileWriter(fc.getSelectedFile())) {
                //Write the jsonarray to file
                fw.write(allpoints.toString(2));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Blir kjørt når brukeren klikker på et punkt i bildet
     * @param mouseEvent event fra klikket som ble gjort
     */
    public void findPos(MouseEvent mouseEvent) {
        //Get the point where the user clicked
        Point p = new Point(mouseEvent.getX(), mouseEvent.getY());

        //Update the shape on the GUI
        currentshape.getPoints().addAll(p.getX(), p.getY());

        //add the point to the list in the datastructure
        currentpoint.add(p);


    }

    /**
     * Creates a new polygon which displpayes the shape of where the user have clicked
     */
    private void drawShape() {
        //reset the polygon
        currentshape = new Polygon();
        //set style
        currentshape.setStroke(Color.BLACK);
        currentshape.setFill(Color.color(0.153,0.98,0.176,0.3));
        //add it to the GUI
        imageContainer.getChildren().add(currentshape);
    }
}
