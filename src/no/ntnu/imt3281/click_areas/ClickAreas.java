package no.ntnu.imt3281.click_areas;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


/**
 * Oppgave 9
 *
 */
public class ClickAreas {
    /*
    ##### FXML VARIABLE ######
     */
    @FXML
    public Label areaToClick;
    @FXML
    public TextArea results;

    /*
    ##### KLASSEVARIABLE #####
     */
    //JSON file to be read
    private final String JSONFILE = "former.json";

    //Hashmap to store all the shapes and name of the polygon
    private HashMap<String, Polygon> shapes = new HashMap<>();
    private Random r = new Random();

    private String shapetoclick;
    //current amount of correct shapes
    private int numberofshapes = 0;
    //The total shapes in the current structure
    private int totalshapes;
    /**
     * This function is ran before the GUI is displayed
     */
    public void initialize(){
        //Read and parse the json file
        readJSON();
        //Select a shape to be clicked
        randomShape();
        areaToClick.setText(shapetoclick);
        //Find out the total number of shapes in the datastructure
        totalshapes = shapes.size();
    }

    /**
     * Finds a random shape for the user to click on
     */
    private void randomShape() {
        //If the hashmap contains at least one element
        if (shapes.size() > 0) {

            //This snippet is inspired from:
                //https://stackoverflow.com/questions/929554/is-there-a-way-to-get-the-value-of-a-hashmap-randomly-in-java
            Object[] keys = shapes.keySet().toArray();
            //Select a random key from the list of all the avaliable keys
            shapetoclick = (String) keys[r.nextInt(keys.length)];
            System.out.println(shapetoclick);
        }
    }

    /**
     * Read the json structure from the file
     */
    private void readJSON() {
        //Open the file
        try (BufferedReader buffread = new BufferedReader(new FileReader(JSONFILE))) {
            StringBuffer stringbuf = new StringBuffer();           //Read/append the file/text into a stringbuffer:
            String line;
            while ((line = buffread.readLine()) != null) {
                stringbuf.append(line);
            }

            //Get the outer array
            JSONArray mainarray = new JSONArray(stringbuf.toString());

            //Parse all the objects inside
            for (int i = 0; i < mainarray.length(); i++) {
                JSONObject arrayobject = mainarray.getJSONObject(i);
                parseObject(arrayobject);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parse the shape (object) in the jsonarray of all the objects
     * @param arrayobject
     */
    private void parseObject(JSONObject arrayobject) {
        String shapename = arrayobject.getString("name");
        JSONArray coordinates = arrayobject.getJSONArray("polygon");
        Point[] polygoncoordinates = new Point[coordinates.length()];

        //Put all the coordinates into a array of points
        for (int i = 0; i < coordinates.length(); i++) {
            polygoncoordinates[i] = new Point(coordinates.getJSONObject(i).getDouble("x"), coordinates.getJSONObject(i).getDouble("y"));
        }

        //Add the object to the hashmap of all the shapes
        shapes.put(shapename, new Polygon(polygoncoordinates));
    }

    /**
     * This method is called when a user clicks on a certain point on the image
     *
     * @param mouseEvent
     */
    public void clicked(MouseEvent mouseEvent) {
        //Get the point from the place where the user clicked
        Point p = new Point(mouseEvent.getX(), mouseEvent.getY());
        //Get the correct object that corresponds to the current shape
        Polygon poly = shapes.get(shapetoclick);

        //Only proceed if there are shapes left to click
        if(shapes.size() > 0) {
            //If the click is inside the shape (polygon)
            if (poly.contains(p)) {
                //Correct
                results.appendText("Det er helt rett\n");
                //The user completed one more shape
                numberofshapes++;
                //Remove the current shape from the datastructure
                shapes.remove(shapetoclick, poly);

                //If the user have completed all the shapes: print text
                if (numberofshapes == totalshapes) {
                    results.appendText("\nDu har alt rett\n");
                }

                //Select new shape to click
                randomShape();
                areaToClick.setText(shapetoclick);

            } else { //User click was outside the shape
                results.appendText(String.format("Det var dessverre feil, du skal klikke på %s%n", shapetoclick));
            }
        }
    }
}