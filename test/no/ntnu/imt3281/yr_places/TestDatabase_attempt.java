package no.ntnu.imt3281.yr_places;
import static org.junit.Assert.*;
import no.ntnu.imt3281.logic.Database_attempt;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static no.ntnu.imt3281.yr_places.GetPlaces.hentSteder;


public class TestDatabase_attempt {

    static Database_attempt db;

    @BeforeClass
    public static void setup(){
        db = Database_attempt.getTempDatabase();
    }

    @Test
    public void testInsertNewPlace(){
        Place place;
        List<List<String>> places = hentSteder();
        place = new Place(places.get(1));
        db.insertNewPlace(place);
    }

    @Test
    public void testFindClosestLocation(){
        //Insert all the places to populate the DB with data
        Place place;
        List<List<String>> places = hentSteder();
        for (int i = 1; i < places.size(); i++) {
            place = new Place(places.get(i));
            db.insertNewPlace(place);
        }

        Place closestplace = db.findClosestPlace(59.14465, 11.45458);
        String stedsnavn = "Asak kirke";
        String stedstype = "Kirke";
        assertEquals(stedsnavn,closestplace.getStedsnavn());
        assertEquals(stedstype, closestplace.getStedstype());
    }
}
