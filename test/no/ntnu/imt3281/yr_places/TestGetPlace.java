package no.ntnu.imt3281.yr_places;

import static org.junit.Assert.*;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Oppgave 2
 * Tests to see if the downloaded data contains the right elements
 */
public class TestGetPlace {

    /**
     * Testv to see if the function returns the right header of the file in the right order
     */
    @Test
    public void testHentSteder(){
        List<List<String>> teststeder = GetPlaces.hentSteder();
        List<String> stringlisttype = new ArrayList<>(Arrays.asList("Kommunenummer", "Stadnamn","Prioritet","Stadtype nynorsk","Stadtype bokmål","Stadtype engelsk","Kommune","Fylke","Lat","Lon","Høgd","Nynorsk","Bokmål","Engelsk"));

        //test to see if the header contains the right elements on the page
        assertEquals(stringlisttype, teststeder.get(0));

        for (int outer = 0; outer < teststeder.size(); outer++) {
            //Test that the listelements are not empty
            assertNotNull(teststeder.get(outer));
            for (int inner = 0; inner < teststeder.get(outer).size() ; inner++) {
                //Test that the String elements are not empty
                assertNotNull(teststeder.get(outer).get(inner));
                //Test that the size of the List<String> are larger than 12
                assertTrue(teststeder.get(outer).size() >= 13);
            }
        }

    }


}
